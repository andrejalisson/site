<!DOCTYPE html>
<html dir="ltr" lang="pt-BR">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Poppins:300,400,500,600,700|PT+Serif:400,400i&display=swap" rel="stylesheet" type="text/css" />
	<!--Favicon-->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

	<link rel="stylesheet" href="{{asset('site/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('site/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('site/css/swiper.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('site/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('site/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('site/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('site/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('site/css/custom.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Home | Fábrica C</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header transparent-header dark">
			<div id="header-wrap">
				<div class="container">
					<div class="header-row">

						<!-- Logo
						============================================= -->
						<div id="logo">
							<a href="index.html" class="standard-logo" data-dark-logo="site/images/logo-dark.png"><img src="{{asset('site/images/logo.png')}}" alt="Canvas Logo"></a>
							<a href="index.html" class="retina-logo" data-dark-logo="site/images/logo-dark@2x.png"><img src="{{asset('site/images/logo@2x.png')}}" alt="Canvas Logo"></a>
						</div><!-- #logo end -->

						<div class="header-misc">

							<!-- Top Search
							============================================= -->
							<div id="top-search" class="header-misc-icon">
								<a href="#" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
							</div><!-- #top-search end -->


						</div>

						<div id="primary-menu-trigger">
							<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
						</div>

						<!-- Primary Navigation
						============================================= -->
						<nav class="primary-menu">

							<ul class="menu-container">
							
                                <li class="menu-item">
                                   <a class="menu-link" href="#"><div>Início</div></a>
                                </li>
                                <li class="menu-item">
                                    <a class="menu-link"  href="#"><div>Módulos</div></a>
                                </li>
                                <li class="menu-item">
                                    <a class="menu-link" href="#"><div>Serviços</div></a>
                                </li>
                                <li class="menu-item">
                                    <a class="menu-link" href="#"><div>Depoimentos</div></a>
                                </li>
                                <li class="menu-item">
                                    <a class="menu-link" href="#"><div>Contato</div></a>
                                </li>

                            </ul>

						</nav><!-- #primary-menu end -->

						<form class="top-search-form" action="search.html" method="get">
							<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
						</form>

					</div>
				</div>
			</div>
			<div class="header-wrap-clone"></div>
		</header><!-- #header end -->

		<section id="slider" class="slider-element slider-parallax include-header min-vh-100" style="background: url('{{asset('site/images/landing/landing1.jpg')}}') no-repeat; background-size: cover">
			<div class="slider-inner">

				<div class="vertical-middle slider-element-fade">
					<div class="container dark text-center">

						<div class="heading-block mb-0 center">
							<h1>
								<span class="text-rotater nocolor" data-separator="|" data-rotate="flipInX" data-speed="3500">
									Conheça o caminho da sua <span class="t-rotate">liberdade Financeira|Carreira de Sucesso|Nova profissão</span>
								</span>
							</h1>
							<span class="d-none d-md-block">Inscreva-se agora para nosso workshop gratuito</span>
						</div>

						<a href="https://wa.me/5585997801608" target="_blank" class="button button-border button-light button-rounded button-reveal text-end button-large topmargin d-none d-md-inline-block"><i class="icon-angle-right"></i><span>Inscreva-se AGORA</span></a>

					</div>
				</div>

			</div>
		</section>

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">

				<div class="promo promo-light promo-full bottommargin-lg header-stick border-top-0 p-5">
					<div class="container clearfix">
						<div class="row align-items-center">
							<div class="col-12 col-lg">
								<h3>Inscreva-se agora para o próximo workshop gratuitamente <span>{{date('d/m/Y', strtotime("next Saturday"));}}</span></h3>
								<span>Você conhecerá sua nova profissão e em até 2 meses estará ganhando dinheiro através dos seus aprendizados</span>
							</div>
							<div class="col-12 col-lg-auto mt-4 mt-lg-0">
								<a href="https://wa.me/5585997801608" target="_blank" class="button button-large button-circle m-0">Inscreva-se AGORA</a>
							</div>
						</div>
					</div>
				</div>

				<div class="container clearfix">

					<div class="row col-mb-50">
						<div class="col-sm-6 col-lg-2">
							<div class="feature-box fbox-center fbox-light fbox-effect border-bottom-0">
								<div class="fbox-icon">
									<a id="mod1" href="#"><i class="icon-desktop1"></i></a>
								</div>
								<div class="fbox-content">
									<h3>ARQUITETURA E MANUTENÇÃO DE COMPUTADORES<span class="subtitle"></span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-2">
							<div class="feature-box fbox-center fbox-light fbox-effect border-bottom-0">
								<div class="fbox-icon">
									<a href="#"><i class="icon-settings"></i></a>
								</div>
								<div class="fbox-content">
									<h3>INSTALAÇÃO E CONFIGURAÇÃO DE SISTEMAS OPERACIONAIS<span class="subtitle"></span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-2">
							<div class="feature-box fbox-center fbox-light fbox-effect border-bottom-0">
								<div class="fbox-icon">
									<a href="#"><i class="icon-code-fork"></i></a>
								</div>
								<div class="fbox-content">
									<h3>ELETRÔNICA ESSENCIAL E LINEAR<span class="subtitle"></span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-2">
							<div class="feature-box fbox-center fbox-light fbox-effect border-bottom-0">
								<div class="fbox-icon">
									<a href="#"><i class="icon-mixcloud"></i></a>
								</div>
								<div class="fbox-content">
									<h3>REDES DE COMPUTADORES<span class="subtitle"></span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-2">
							<div class="feature-box fbox-center fbox-light fbox-effect border-bottom-0">
								<div class="fbox-icon">
									<a href="#"><i class="icon-laptop2"></i></a>
								</div>
								<div class="fbox-content">
									<h3>ARQUITETURA E MANUTENÇÃO DE NOTEBOOKS<span class="subtitle"></span></h3>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-lg-2">
							<div class="feature-box fbox-center fbox-light fbox-effect border-bottom-0">
								<div class="fbox-icon">
									<a href="#"><i class="icon-hand-holding-usd"></i></a>
								</div>
								<div class="fbox-content">
									<h3>Empreendedorismo<span class="subtitle"></span></h3>
								</div>
							</div>
						</div>
					</div>

					<div class="line"></div>

					<div class="row col-mb-50">
						<div class="col-md-5">
							<a href="https://www.youtube.com/watch?v=kgIx0zYVJfk" class="d-block position-relative rounded overflow-hidden" data-lightbox="iframe">
								<img src="{{asset('site/images/others/1.jpg')}}" alt="Image" class="w-100">
								<div class="bg-overlay">
									<div class="bg-overlay-content dark">
										<i class="i-circled i-light icon-line-play m-0"></i>
									</div>	
									<div class="bg-overlay-bg op-06 dark"></div>
								</div>
							</a>
						</div>

						<div class="col-md-7">
							<div class="">
								<h2>Geramos resultado através do ensino.</h2>
							</div>

							<p>Estamos cansados de ver jovens frustrados após concluir um curso e não conseguir vaga no mercado de trabalho. 
							A Fábrica C, surge dessa deficiência das escolas de não capacitar bem seus alunos, não preparando-os para a realidade do mercado hoje! 
							Com aulas 100% PRÁTICAS, desde o primeiro dia de aula, o aluno começa a sentir o ambiente profissional como realmente é. 
							Um laboratório de aulas práticas totalmente equipado, com todos os materiais que possam vir a utilizar no mercado, professores altamente capacitados e 
							atuantes na área, garantem um ensino técnico atual e de qualidade. Assim, ao concluir o curso o aluno terá bagagem 
							de conhecimento não só para trabalhar em uma empresa, mas também se quiser poderá montar seu próprio negócio de assistência e manutenção de computadores.</p>
							
							<!-- 	
							<p>Métodos desenhados para acelerar a sua evolução.</p>

							<div class="row col-mb-30">
								<div class="col-sm-6 col-md-12 col-lg-6">
									<ul class="iconlist iconlist-color mb-0">
										<li><i class="icon-caret-right"></i> Manutenção de computadores e notebooks</li>
										<li><i class="icon-caret-right"></i> Criação de sites</li>
										<li><i class="icon-caret-right"></i> Introdução a programação</li>
										<li><i class="icon-caret-right"></i> Redes de Computadores</li>
									</ul>
								</div>

								<div class="col-sm-6 col-md-12 col-lg-6">
									<ul class="iconlist iconlist-color mb-0">
										<li><i class="icon-caret-right"></i> Git e GitHub</li>
										<li><i class="icon-caret-right"></i> Empreendedorismo</li>
										<li><i class="icon-caret-right"></i> Designer gráfico</li>
										<li><i class="icon-caret-right"></i> Criação de e-Commerce</li>
									</ul>
								</div>
							</div>-->
						</div>
					</div>

				</div>

				<div class="section topmargin-lg">
					<div class="container clearfix">

						<div class="heading-block center">
							<span class="text-rotater nocolor" data-separator="|" data-rotate="flipInX" data-speed="3500">
							<h2>Conheça o caminho da sua <span class="t-rotate">Liberdade financeira|Nova profissão|Carreira de sucesso</span>
							</span></h2>
							<span>Onde começa sua jornada em busca do sucesso!</span>
						</div>

						<div class="row justify-content-center col-mb-50">
							<div class="col-sm-6 col-lg-6">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn">
									<div class="fbox-icon">
										<a href="#"><i class="icon-desktop1"></i></a>
									</div>
									<div class="fbox-content">
										<h3>Arquitetura e manutenção de computadores</h3>
										<p>O aluno terá contato com conceitos fundamentais para iniciar sua jornada como um profissional técnico em hardware de sucesso!</p>
										<p>Será abordado todos os componentes internos do computar e enfatizando sempre os componentes triviais que são necessários em todo computador, independente se 
											for uma máquina game ou servidor de alto desempenho ou até mesmo um PC mais simples.
E sempre com aulas práticas em laboratório equipado com as reais ferramentas utilizadas para resolverem de fato os problemas que existem no mercado em relação manutenção de computadores.
Este cenário acelera ainda mais o aprendizado e compreensão do aluno.</p>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-lg-6">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="200">
									<div class="fbox-icon">
										<a href="#"><i class="icon-settings"></i></a>
									</div>
									<div class="fbox-content">
										<h3>INSTALAÇÃO E CONFIGURAÇÃO DE SISTEMAS OPERACIONAIS</h3>
										<p>Após dominar de forma concreta o Hardware do computador, é apresentado o próximo desafio: Softwares!</p>
										<p>O computador não teria tantas vantagens se não fosse um sistema operacional, para gerenciar de forma eficiente todos os recursos que nos trazem. 
											Então é importantíssimo um técnico dominar a instalação e configuração de sistemas operacionais, sejam nos mais diversos ambientes como Windows, Linux e MacOs. 
											Em nossas aulas você terá todo suporte e prática necessária para dominar todos os conceitos demandas sobre sistemas operacionais.
</p>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-lg-6">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="400">
									<div class="fbox-icon">
										<a href="#"><i class="icon-code-fork"></i></a>
									</div>
									<div class="fbox-content">
										<h3>ELETRÔNICA ESSENCIAL E LINEAR</h3>
										<p>Encerrando os Desafios do primeiro ciclo, o aluno se depara ainda com eletrônica!</p>
										<p>Todos os componentes internos do computador nada mais são, do que circuitos eletrônicos integrados, de forma que juntos são capazes coisa fantásticas. 
											Abordaremos a eletrônica em situações reais, solucionando problemas nos componentes microeletrônicos de placas - mãe, fontes e outros. Assim ao final do módulo, 
											a prática de solda e sucção se torna um recurso diferenciado ofertado aos alunos.</p>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-lg-6">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="600">
									<div class="fbox-icon">
										<a href="#"><i class="icon-mixcloud"></i></a>
									</div>
									<div class="fbox-content">
										<h3>REDES DE COMPUTADORES</h3>
										<p>O módulo de redes será apresentado de forma clara e objetiva, alinhando os conceitos de topologias  e implementações de redes diretamente na prática.</p>
									    <p>Toda a estrutura de redes, desde o início do primeiro cabo RJ-45 à configuração de IPs. Novamente com todo o suporte de estrutura, ferramentas e 
											acompanhamento técnico, para garantir a melhor experiência no aprendizado.</p>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-lg-6">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="800">
									<div class="fbox-icon">
										<a href="#"><i class="icon-laptop2"></i></a>
									</div>
									<div class="fbox-content">
										<h3>ARQUITETURA E MANUTENÇÃO DE NOTEBOOKS</h3>
										<p>Toda a bagagem de conhecimento adquirido no curso se torna essencial e extremamente útil para desenvolvimento do aluno.</p>
										<p>Esse módulo é focado totalmente em práticas na resolução de problemas reais em notebooks, como trocas de tela, hds, memórias, 
											teclados, limpeza completa em placas- mães e etc. 
Pois o notebook apresenta os mesmos componentes de um computador, mas em uma arquitetura reduzida, compacta, destinada a portabilidade.</p>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-lg-6">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="1000">
									<div class="fbox-icon">
										<a href="#"><i class="icon-hand-holding-usd"></i></a>
									</div>
									<div class="fbox-content">
										<h3>Empreenda</h3>
										<p>O curso de HARDWARE proporciona liberdade, escolha. Ao concluir o curso o aluno se depara com a possibilidade de um emprego tradicional ou empreender.</p>
										<p>Vou compartilhar o passo-a-passo para conquistar seu primeiro cliente. Esclarecer pontos decisivos em relação ao CNPJ, MEI, LTDA, TAXAS e etc. 
											Acessória real com contador e equipe administrativa.</p>
									</div>
								</div>
							</div>

							
						<!--- 
							<div class="col-sm-6 col-lg-4">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="1200">
									<div class="fbox-icon">
										<a href="#"><i class="icon-bulb"></i></a>
									</div>
									<div class="fbox-content">
										<h3>Light &amp; Dark Color Schemes</h3>
										<p>Change your Website's Primary Scheme instantly by simply adding the dark class to the body.</p>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-lg-4">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="1400">
									<div class="fbox-icon">
										<a href="#"><i class="icon-heart2"></i></a>
									</div>
									<div class="fbox-content">
										<h3>Boxed &amp; Wide Layouts</h3>
										<p>Stretch your Website to the Full Width or make it boxed to surprise your visitors.</p>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-lg-4">
								<div class="feature-box fbox-sm fbox-plain" data-animate="fadeIn" data-delay="1600">
									<div class="fbox-icon">
										<a href="#"><i class="icon-note"></i></a>
									</div>
									<div class="fbox-content">
										<h3>Extensive Documentation</h3>
										<p>We have covered each &amp; everything in our Documentation including Videos &amp; Screenshots.</p>
									</div>
								</div>
							</div>
						</div> -->  

					</div>
				</div>

				<div class="container clearfix">

					<div class="heading-block center">
					<span class="text-rotater nocolor" data-separator="|" data-rotate="flipInX" data-speed="3500">
							<h2>Venha fazer parte do nosso <span class="t-rotate">Time|Propósito|Cowork</span>
							</span></h2>
						<span>Criamos um projeto que realmente nos orgulhamos.</span>
					</div>

					<div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="1" data-pagi="false" data-autoplay="5000" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-xl="4">

						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="{{asset('site/images/portfolio/4/1.jpg')}}" alt="Open Imagination">
								</a>
								<div class="bg-overlay">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn" data-hover-speed="350">
										<a href="{{asset('site/images/portfolio/full/1.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeInUpSmall" data-hover-speed="350" data-lightbox="image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeInUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn" data-hover-speed="350"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">TURMA REUNIDA</a></h3>
								<span><a href="#">Media</a>, <a href="#">Icons</a></span>
							</div>
						</div>

						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="{{asset('site/images/portfolio/4/2.jpg')}}" alt="Locked Steel Gate">
								</a>
								<div class="bg-overlay">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn" data-hover-speed="350">
										<a href="{{asset('site/images/portfolio/full/2.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeInUpSmall" data-hover-speed="350" data-lightbox="image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeInUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn" data-hover-speed="350"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Locked Steel Gate</a></h3>
								<span><a href="#">Illustrations</a></span>
							</div>
						</div>
						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="#">
									<img src="{{asset('site/images/portfolio/4/3.jpg')}}" alt="Mac Sunglasses">
								</a>
								<div class="bg-overlay">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn" data-hover-speed="350">
										<a href="https://vimeo.com/89396394" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeInUpSmall" data-hover-speed="350" data-lightbox="iframe"><i class="icon-line-play"></i></a>
										<a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeInUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn" data-hover-speed="350"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-video.html">Mac Sunglasses</a></h3>
								<span><a href="#">Graphics</a>, <a href="#">UI Elements</a></span>
							</div>
						</div>
						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="#">
									<img src="{{asset('site/images/portfolio/4/4.jpg')}}" alt="Morning Dew">
								</a>
								<div class="bg-overlay" data-lightbox="gallery">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn">
										<a href="{{asset('site/images/portfolio/full/4.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
										<a href="{{asset('site/images/portfolio/full/4-1.jpg')}}" class="d-none" data-lightbox="gallery-item"></a>
										<a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-gallery.html">Morning Dew</a></h3>
								<span><a href="#">Icons</a>, <a href="#">Illustrations</a></span>
							</div>
						</div>
						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="{{asset('site/images/portfolio/4/5.jpg')}}" alt="Console Activity">
								</a>
								<div class="bg-overlay">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn">
										<a href="{{asset('site/images/portfolio/full/5.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="image" title="Image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Console Activity</a></h3>
								<span><a href="#">UI Elements</a>, <a href="#">Media</a></span>
							</div>
						</div>
						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="portfolio-single-gallery.html">
									<img src="{{asset('site/images/portfolio/4/6.mp4')}}" alt="Shake It!">
								</a>
								<div class="bg-overlay" data-lightbox="gallery">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn">
										<a href="{{asset('site/images/portfolio/full/6.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
										<a href="{{asset('site/images/portfolio/full/6-1.jpg')}}" class="d-none" data-lightbox="gallery-item"></a>
										<a href="{{asset('site/images/portfolio/full/6-2.jpg')}}" class="d-none" data-lightbox="gallery-item"></a>
										<a href="{{asset('site/images/portfolio/full/6-3.jpg')}}" class="d-none" data-lightbox="gallery-item"></a>
										<a href="portfolio-single-gallery.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-gallery.html">Shake It!</a></h3>
								<span><a href="#">Illustrations</a>, <a href="#">Graphics</a></span>
							</div>
						</div>
						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="portfolio-single-video.html">
									<img src="{{asset('site/images/portfolio/4/7.jpg')}}" alt="Backpack Contents">
								</a>
								<div class="bg-overlay">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn">
										<a href="https://www.youtube.com/watch?v=kuceVNBTJio" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="iframe"><i class="icon-line-play"></i></a>
										<a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single-video.html">Backpack Contents</a></h3>
								<span><a href="#">UI Elements</a>, <a href="#">Icons</a></span>
							</div>
						</div>
						<div class="portfolio-item">
							<div class="portfolio-image">
								<a href="portfolio-single.html">
									<img src="{{asset('site/images/portfolio/4/8.jpg')}}" alt="Sunset Bulb Glow">
								</a>
								<div class="bg-overlay">
									<div class="bg-overlay-content dark" data-hover-animate="fadeIn">
										<a href="{{asset('site/images/portfolio/full/8.jpg')}}" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350" data-lightbox="image" title="Image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="overlay-trigger-icon bg-light text-dark" data-hover-animate="fadeInDownSmall" data-hover-animate-out="fadeOutUpSmall" data-hover-speed="350"><i class="icon-line-ellipsis"></i></a>
									</div>
									<div class="bg-overlay-bg dark" data-hover-animate="fadeIn"></div>
								</div>
							</div>
							<div class="portfolio-desc">
								<h3><a href="portfolio-single.html">Sunset Bulb Glow</a></h3>
								<span><a href="#">Graphics</a></span>
							</div>
						</div>

					</div>
				</div>

				<div class="section topmargin-sm mb-0">

					<div class="container clearfix">

						<div class="heading-block center">
							<h3>Depoimentos</h3>
							<span>O que alguns dos nossos alunos acham da Fábrica C!</span>
						</div>

						<ul class="testimonials-grid grid-1 grid-md-2 grid-lg-3">
							<li class="grid-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="{{asset('site/images/testimonials/1.jpg')}}" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p> Comecei o curso de manutenção de computadores e em pouco tempo já estava resolvendo problemas reais.</p>
										<div class="testi-meta">
											João vitor
											<span></span>
										</div>
									</div>
								</div>
							</li>
							<li class="grid-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="{{asset('site/images/testimonials/2.jpg')}}" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Fui encaminhado pra uma entrevista como assistente de TI, consegui o estágio e hoje já estou efetivado trabalhando de carteira assinada mesmo!</p>
										<div class="testi-meta">
											Deivid
											<span></span>
										</div>
									</div>
								</div>
							</li>
							<li class="grid-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="{{asset('site/images/testimonials/7.jpg')}}" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>As aulas práticas de notebook foram realmente importantes, pois pude aplicar realmente na prática na minha empresa resolvendo
											os problemas que apareciam. </p>
										<div class="testi-meta">
											Jully
											<span></span>
										</div>
									</div>
								</div>
							</li>
							<li class="grid-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="{{asset('site/images/testimonials/3.jpg')}}" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Sempre gostei de tecnologia e estou me deparando com um mundo que eu nem imagina a proporção, o conhecimento é libertador!</p>
										<div class="testi-meta">
											Mauro
											<span></span>
										</div>
									</div>
								</div>
							</li>
							<li class="grid-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="{{asset('site/images/testimonials/4.jpg')}}" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>Desde o primeiro dia de aula tivemos aula prática. Todo o conteúdo do curso é voltando para mundo real. Comecei a atender os clientes
											com a ajuda do professor que foi fundamental pra eu ter mais confiança em resolver os problemas.
										</p>
										<div class="testi-meta">
											Aluno Master
											<span></span>
										</div>
									</div>
								</div>
							</li>
							<li class="grid-item">
								<div class="testimonial">
									<div class="testi-image">
										<a href="#"><img src="{{asset('site/images/testimonials/8.jpg')}}" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>O professor sempre trás problemas reais, com notebooks, câmeras de segurança e até celulares.</p>
										<div class="testi-meta">
											João Miguel
											<span></span>
										</div>
									</div>
								</div>
							</li>
						</ul>

					</div>

				</div>

				<!--<div class="container clearfix">

					<div id="oc-clients" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-loop="true" data-nav="false" data-autoplay="5000" data-pagi="false" data-items-xs="2" data-items-sm="3" data-items-md="4" data-items-lg="5" data-items-xl="6" style="padding: 20px 0;">

						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/1.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/2.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/3.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/4.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/5.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/6.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/7.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/8.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/9.png')}}" alt="Clients"></a></div>
						<div class="oc-item"><a href="http://logofury.com/"><img src="{{asset('site/images/clients/10.png')}}" alt="Clients"></a></div>

					</div>

				</div>-->

				<a href="https://wa.me/5585997801608" target="_blank" class="button button-full center text-end footer-stick">
					<div class="container clearfix"><span>COMEÇAR AGORA</span>
						</div>
				</a>

			</div>
		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">
			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap">

					<div class="row col-mb-50">
						<div class="col-lg-8">

							<div class="row col-mb-50">
								<div class="col-md-4">

									<div class="widget clearfix">

										<img src="{{asset('site/images/footer-widget-logo.png')}}" alt="Image" class="footer-logo">


										<div style="background: url('{{asset('site/images/world-map.png')}}') no-repeat center center; background-size: 100%;">
											<address>
												<strong>Endereço:</strong><br>
												AV. Do Imperador, Nº 852<br>
												Centro, Fortaleza-CE<br>
											</address>
											<abbr title="Telefone"><strong>Telefone:</strong></abbr> (85) 3221-3501<br>
											<abbr title="E-mail"><strong>E-mail:</strong></abbr> contato@fabricac.tec.br
										</div>

									</div>

								</div>

								<div class="col-md-4">

									<div class="widget widget_links clearfix">

										<h4>Sitemap</h4>

										<ul>
											<li><a href="#">Home</a></li>
											<li><a href="#">Sobre</a></li>
											<li><a href="#">Serviços</a></li>
											<li><a href="#">Depoimentos</a></li>
											<li><a href="#">Contato</a></li>
										</ul>

									</div>

								</div>

								<div class="col-md-4">

									<div class="widget clearfix">
										<h4>Posts Recentes</h4>

										<div class="posts-sm row col-mb-30" id="post-list-footer">
											<div class="entry col-12">
												<div class="grid-inner row">
													<div class="col">
														<div class="entry-title">
															<h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
														</div>
														<div class="entry-meta">
															<ul>
																<li>10th July 2021</li>
															</ul>
														</div>
													</div>
												</div>
											</div>

											<div class="entry col-12">
												<div class="grid-inner row">
													<div class="col">
														<div class="entry-title">
															<h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
														</div>
														<div class="entry-meta">
															<ul>
																<li>10th July 2021</li>
															</ul>
														</div>
													</div>
												</div>
											</div>

											<div class="entry col-12">
												<div class="grid-inner row">
													<div class="col">
														<div class="entry-title">
															<h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
														</div>
														<div class="entry-meta">
															<ul>
																<li>10th July 2021</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>

						<div class="col-lg-4">

							<div class="row col-mb-50">
								<div class="col-md-5 col-lg-12">
									<div class="widget subscribe-widget clearfix">
										<h5><strong>Assine</strong> nosso boletim informativo para receber notícias importantes, ofertas incríveis e informações privilegiadas:</h5>
										<div class="widget-subscribe-form-result"></div>
										<form id="widget-subscribe-form" action="#" method="post" class="mb-0">
											<div class="input-group mx-auto">
												<div class="input-group-text"><i class="icon-email2"></i></div>
												<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Seu e-mail">
												<button class="btn btn-success" type="submit">Assinar</button>
											</div>
										</form>
									</div>
								</div>
                                <div class="col-md-4 col-lg-12">
									<div class="widget clearfix" style="margin-bottom: -20px;">

										<div class="row">
											<div class="col-lg-6 bottommargin-sm">
												<div class="counter counter-small"><span data-from="50" data-to="+5000" data-refresh-interval="80" data-speed="3000" data-comma="true"></span></div>
												<h5 class="mb-0">Alunos Formados</h5>
											</div>

											<div class="col-lg-6 bottommargin-sm">
												<div class="counter counter-small"><span data-from="100" data-to="+60" data-refresh-interval="50" data-speed="2000" data-comma="true"></span></div>
												<h5 class="mb-0">Cursos</h5>
											</div>
										</div>

									</div>
								</div>

								<div class="col-md-3 col-lg-12">
									<div class="widget clearfix" style="margin-bottom: -20px;">

										<div class="row">
											<div class="col-6 col-md-12 col-lg-6 clearfix bottommargin-sm">
												<a href="#" class="social-icon si-dark si-colored si-facebook mb-0" style="margin-right: 10px;">
													<i class="icon-facebook"></i>
													<i class="icon-facebook"></i>
												</a>
												<a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
											</div>
											<div class="col-6 col-md-12 col-lg-6 clearfix">
												<a href="#" class="social-icon si-dark si-colored si-rss mb-0" style="margin-right: 10px;">
													<i class="icon-rss"></i>
													<i class="icon-rss"></i>
												</a>
												<a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
											</div>
										</div>

									</div>
								</div>

							</div>

						</div>
					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">
				<div class="container">

					<div class="row col-mb-30">

						<div class="col-md-7 text-center text-md-start">
							Copyrights &copy; {{date('Y')}} Todos os direitos reservados. Fabrica C.<br>
							<div class="copyright-links">FABRICA C - COMERCIO E SERVICOS DE EQUIPAMENTOS ELETRONICOS LTDA | CNPJ: 40.433.007/0001-11</div>
						</div>

						<div class="col-md-5 text-center text-md-end">
							<div class="d-flex justify-content-center justify-content-md-end">
								<a href="https://www.facebook.com/Fabricac.tec" target="_blank" class="social-icon si-small si-borderless si-facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>

								<a href="https://www.instagram.com/fabricac.tec" target="_blank" class="social-icon si-small si-borderless si-instagram">
									<i class="icon-instagram"></i>
									<i class="icon-instagram"></i>
								</a>

								<a href="#" target="_blank" class="social-icon si-small si-borderless si-linkedin">
									<i class="icon-linkedin"></i>
									<i class="icon-linkedin"></i>
								</a>
							</div>

							<div class="clear"></div>

						 	<i class="icon-envelope2"></i> contato@fabricac.tec.br <span class="middot">&middot;</span> <i class="icon-headphones"></i> (85) 3221-3501
						</div>

					</div>

				</div>
			</div><!-- #copyrights end -->
		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	<script src="{{asset('site/js/jquery.js')}}"></script>
	<script src="{{asset('site/js/plugins.min.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="{{asset('site/js/functions.js')}}"></script>

</body>
</html>
